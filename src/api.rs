use std::fmt::Display;

use reqwasm::http::Request;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Clone, Eq, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Input {
    pub content: String,
    pub content_type: String,
    pub preset: Preset,
}

#[derive(Serialize, Clone, Copy, Eq, PartialEq, Debug)]
pub enum Preset {
    #[serde(rename = "ko-kr")]
    KoKR,
    #[serde(rename = "ko-kp")]
    KoKP,
}

impl Display for Preset {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Preset::KoKR => write!(f, "South Korean"),
            Preset::KoKP => write!(f, "North Korean"),
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(untagged)]
enum RawOutput {
    #[serde(rename_all = "camelCase")]
    Success {
        #[serde(rename = "success")]
        _success: bool,
        #[serde(rename = "warnings")]
        _warnings: Vec<String>,
        content: String,
        content_type: String,
    },
    #[serde(rename_all = "camelCase")]
    Error {
        #[serde(rename = "success")]
        _success: bool,
        message: String,
    },
}

#[derive(Debug)]
pub struct Success {
    pub content: String,
    pub content_type: String,
}

#[derive(Debug)]
pub enum Failure {
    SerdeError(serde_json::error::Error),
    HttpError(reqwasm::Error),
    SeonbiError(String),
}

impl From<serde_json::error::Error> for Failure {
    fn from(error: serde_json::error::Error) -> Self {
        Failure::SerdeError(error)
    }
}

impl From<reqwasm::Error> for Failure {
    fn from(error: reqwasm::Error) -> Self {
        Failure::HttpError(error)
    }
}

pub const SEONBI_API_URL: &str = "https://seonbi.herokuapp.com/";

pub async fn request(input: &Input) -> Result<Success, Failure> {
    let serialized = serde_json::to_string(input)?;
    let future = Request::post(SEONBI_API_URL)
        .header("Content-Type", "application/json")
        .body(serialized)
        .send();
    let response = future.await?;
    let body = response.text().await?;
    let output: RawOutput = serde_json::from_str(&body)?;
    match output {
        RawOutput::Success {
            content,
            content_type,
            ..
        } => Ok(Success {
            content,
            content_type,
        }),
        RawOutput::Error { message, .. } => Err(Failure::SeonbiError(message)),
    }
}
