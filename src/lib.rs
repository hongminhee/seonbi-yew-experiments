use std::rc::Rc;

use tracing::{debug, instrument, warn};
use yew::{function_component, html, use_state, use_state_eq, Callback};

pub mod api;
pub mod input_text;
pub mod tabs;

use crate::api::{request, Failure, Input, Preset, Success};
use crate::input_text::InputText;
use crate::tabs::Tabs;

#[instrument]
#[function_component(App)]
pub fn app() -> Html {
    let loaded = use_state(|| false);
    let last_input = use_state_eq(|| Input {
        content: String::new(),
        content_type: "text/plain".to_string(),
        preset: Preset::KoKR,
    });
    let input = use_state_eq(|| {
        let mut i = (*last_input).to_owned();
        i.content = "本 웹 앱은 러스트 言語와 웹 어셈블리 技術을 利用해 開發된 實驗的인 <선비> 프론트엔드입니다.".to_string();
        i
    });
    let on_change = {
        let input = input.to_owned();
        Callback::from(move |value| {
            let mut i = (*input).to_owned();
            i.content = value;
            input.set(i);
        })
    };
    let loading = use_state(|| false);
    let request_nonce = use_state(|| 0);
    let result = use_state(|| Ok(String::new()));
    let transform = {
        let last_input = last_input.to_owned();
        let input = input.to_owned();
        let loading = loading.to_owned();
        let result = result.to_owned();
        move || {
            let last_input = last_input.to_owned();
            let input = (*input).to_owned();
            let loading = loading.to_owned();
            let result = result.to_owned();
            let request_nonce = request_nonce.to_owned();
            wasm_bindgen_futures::spawn_local(async move {
                let nonce = *request_nonce + 1;
                request_nonce.set(nonce);
                loading.set(true);
                debug!("Sending request #{}...", nonce);
                let response = request(&input).await;
                if nonce < *request_nonce {
                    debug!(
                        concat!(
                            "One or more another requests were made while ",
                            "this one was in progress.  This old request ",
                            "#{} will be ignored: {:?}"
                        ),
                        nonce, response
                    );
                }
                debug!("reponse #{}: {:?}", nonce, response);
                loading.set(false);
                last_input.set(input);
                result.set(match response {
                    Ok(Success { content, .. }) => Ok(content),
                    Err(Failure::SeonbiError(message)) => Err(message),
                    Err(Failure::HttpError(e)) => {
                        Err(format!("HTTP error: {:?}", e))
                    }
                    Err(Failure::SerdeError(e)) => Err(format!(
                        "Serialization/deserialization error: {:?}",
                        e
                    )),
                });
            });
        }
    };
    let onclick = {
        let transform = transform.to_owned();
        Callback::from(move |_| transform())
    };
    let on_select = {
        let input = input.to_owned();
        Callback::from(move |preset| {
            let mut i = (*input).to_owned();
            i.preset = preset;
            input.set(i);
        })
    };
    if !*loaded {
        transform();
        loaded.set(true);
    }
    let remain_to_transform = &*last_input != &*input || result.is_err();
    debug!("input: {:?}", &*input);
    debug!("result: {:?}", *result);
    let presets = Rc::new(vec![Preset::KoKR, Preset::KoKP]);
    html! {
        <main class="container">
            <h1>{"Seonbi"}</h1>
            <div class="grid">
                <div>
                    <InputText
                        value={(*input).content.to_owned()}
                        invalid={!*loading && result.is_err()}
                        {on_change}
                    />
                </div>
                <article
                     aria-busy={loading.to_string()}
                     aria-invalid={(!*loading && result.is_err()).to_string()}
                >{
                    match (*result).to_owned() {
                        Ok(message) => html! {
                            <p>{message}</p>
                        },
                        Err(message) => html! {
                            <p class="error">{message}</p>
                        },
                    }
                }</article>
            </div>
            <div class="grid">
                <Tabs<Preset>
                    active={(*input).preset}
                    items={presets}
                    {on_select}
                />
            </div>
            <button
                {onclick}
                disabled={!remain_to_transform || *loading}
                aria-busy={loading.to_string()}
            >
                {
                    if *loading { "Transforming\u{2026}" }
                    else if result.is_err() { "Try again" }
                    else if remain_to_transform { "Transform" }
                    else { "Transformed" }
                }
            </button>
        </main>
    }
}
