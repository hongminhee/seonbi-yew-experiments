use tracing::debug;
use wasm_bindgen::JsCast;
use web_sys::{Event, HtmlTextAreaElement, KeyboardEvent};
use yew::{
    function_component, html, use_state, Callback, Properties, UseStateHandle,
};

#[derive(Properties, PartialEq)]
pub struct InputTextProps {
    pub value: String,
    #[prop_or(false)]
    pub invalid: bool,
    #[prop_or_default]
    pub on_change: Callback<String>,
}

fn emit_textarea_change_event<T>(
    event: &T,
    on_change: &Callback<String>,
    last_value: &UseStateHandle<String>,
) where
    T: JsCast,
    T: AsRef<Event>,
{
    let e: &Event = event.as_ref();
    let target: HtmlTextAreaElement = e
        .target()
        .expect("Event should have a target when dispatched")
        .unchecked_into();
    if **last_value != target.value().as_str() {
        debug!("{:?} changed: {:?}", target, target.value());
        on_change.emit(target.value());
        last_value.set(target.value());
    }
}

#[function_component(InputText)]
pub fn input_text(props: &InputTextProps) -> Html {
    let last_value = use_state(|| props.value.to_owned());
    let on_change = props.on_change.to_owned();
    let onchange = {
        let last_value = last_value.to_owned();
        Callback::from(move |e: Event| {
            emit_textarea_change_event(&e, &on_change, &last_value);
        })
    };
    let on_change = props.on_change.to_owned();
    let onkeyup = Callback::from(move |e: KeyboardEvent| {
        emit_textarea_change_event(&e, &on_change, &last_value);
    });
    html! {
        <textarea
            cols=80
            rows=20
            value={props.value.to_owned()}
            aria-invalid={props.invalid.to_string()}
            {onchange}
            {onkeyup}
        />
    }
}
