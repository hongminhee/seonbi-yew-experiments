use std::fmt::Display;
use std::rc::Rc;

use yew::{function_component, html, Callback, Properties};

#[derive(Properties, PartialEq)]
pub struct TabsProps<T>
where
    T: Eq,
    T: Clone,
    T: Display,
{
    pub active: T,
    pub items: Rc<Vec<T>>,
    #[prop_or_default]
    pub on_select: Callback<T>,
}

#[function_component(Tabs)]
pub fn tabs<T>(props: &TabsProps<T>) -> Html
where
    T: 'static,
    T: Eq,
    T: Clone,
    T: Display,
{
    let tabs = props.items.iter().map(|item| {
        let on_change = props.on_select.to_owned();
        let onclick = {
            let item = (*item).to_owned();
            on_change.reform(move |_| item.to_owned())
        };
        let active = *item == props.active;
        html! {
            <button
                class={if active { "contrast" } else { "contrast outline" }}
                {onclick}
            >
                {item}
            </button>
        }
    });
    html! {
        <div class="grid">{for tabs}</div>
    }
}
