use seonbi_yew_experiments::App;
use yew::start_app;

fn main() {
    console_error_panic_hook::set_once();
    tracing_wasm::set_as_global_default();
    start_app::<App>();
}
