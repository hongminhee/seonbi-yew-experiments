
Experimental web frontend for [Seonbi]
======================================

This is an attempt to rewrite [Seonbi]'s web frontend using [Yew], which is
written in Rust and built into WebAssembly.  Take a look on the preview page:

<https://hongminhee.gitlab.io/seonbi-yew-experiments/>

In experiments here, I tried to learn the ways to do the following things
in Rust & Yew & WASM:

- HTTP requests
- JSON serialization and deserialization
- Asynchronous I/O using futures
- Effectful components

Although it's trivial, it is distributed under [GPLv3].

[Seonbi]: https://github.com/dahlia/seonbi
[Yew]: https://yew.rs/
[GPLv3]: https://www.gnu.org/licenses/gpl-3.0.html